<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Email\Set;

include_once 'base.php';

/**
 * @property	 string		$name         The name of the role
 * @property	 string		$description  A longer description of the role
 *
 * @method EmailSet     getParentSet()
 * @method Func_App     App()
 */
class EmailSet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Email');
        
        $this->setDescription('Email');
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('fromaddress'),
            ORM_StringField('fromname'),
            ORM_StringField('replyTo')
            ->setDescription('Reply to'),
            ORM_TextField('recipients')
            ->setDescription('Recipients'),
            ORM_TextField('ccRecipients')
            ->setDescription('CC Recipients'),
            ORM_TextField('bccRecipients')
            ->setDescription('BCC Recipients'),
            ORM_StringField('subject')
            ->setDescription('Subject'),
            ORM_TextField('body')
            ->setDescription('Message body'),
            ORM_TextField('altbody')
            ->setDescription('Alternate text/plain message body'),
            ORM_StringField('status')
            ->setDescription('Status'),
            ORM_StringField('hash')
            ->setDescription('Mail spooler hash'),
            ORM_StringField('message_id')
            ->setDescription('Message-ID header content'),
            ORM_BoolField('private')
            ->setDescription('Private')
        );
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new EmailBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new EmailAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function importFromImapMail(\LibImap_Mail $imapMail)
    {
        /* @var $email Email */
        $email = $this->get($this->message_id->is($imapMail->getMessageID()));
        if (!$email) {
            $email = $this->newRecord();
        }
        $email->setValuesFromImap($imapMail);
        
        $email->save(true); // do not trace
        
        return $email;
    }
}

class EmailBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class EmailAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}

class EmailAfterSendEvent extends \bab_event
{
    public $record;
    
    public function __construct(Email $record){
        $this->record = $record;
    }
}