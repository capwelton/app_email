<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Email\Set;

use bab_functionality;

include_once 'base.php';

/**
 * @property	 string		$name         The name of the role
 * @property	 string		$description  A longer description of the role
 *
 * @method EmailSet     getParentSet()
 * @method \Func_App     App()
 */
class Email extends \app_TraceableRecord
{
    public $mail;
    public $nbRecipientsByMail;
    
    public $lb;
    public $stack;
    public $debug;
    public $log;
    public $statusMessages;
    
    /**
     * use the mail template of this skin
     * @var string
     */
    private $_skin_mail_template = null;
    
    const SUBFOLDER = 'emails';
    
    
    /**
     * @param \ORM_RecordSet $oParentSet The set connected to this record
     */
    public function __construct(\ORM_RecordSet $oParentSet)
    {
        parent::__construct($oParentSet);
        
        $this->nbRecipientsByMail = 1;
        $this->lb = "\n";
        $this->stack = array();
        $this->debug = array();
        $this->statusMessages = array();
        $this->log = '';
    }    
    
    /**
     * Sets the values of the email from the values of the specified imap email.
     *
     * @param \LibImap_Mail $imapMail
     * @return Email
     */
    public function setValuesFromImap(\LibImap_Mail $imapMail)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/calincl.php';
        
        $this->createdOn = $imapMail->getDate();
        $this->createdBy = bab_getUserIdByEmailAndName($imapMail->getFromAddress(), $imapMail->getFromName());
        
        $this->modifiedOn = $this->createdOn;
        $this->modifiedBy = $this->createdBy;
        
        $this->fromaddress = $imapMail->getFromAddress();
        $this->fromname = $imapMail->getFromName();
        $this->replyTo = implode(', ',array_keys($imapMail->getReplyTo()));
        $this->recipients = implode(', ',array_keys($imapMail->getTo()));
        $this->ccRecipients = implode(', ',array_keys($imapMail->getCc()));
        $this->subject = $imapMail->getSubject();
        $this->body = $imapMail->getHtmlContent(); // strip embeded images
        $this->message_id = $imapMail->getMessageID();
        
        $this->save(true); // do not trace
        
        $attachments = $imapMail->getAttachments();
        foreach ($attachments as $attachement) {
            /* @var $attachement \LibImap_MailAttachment */
            $this->importAttachment($attachement->filename, $attachement->data);
        }
        
        return $this;
    }
    
    /**
     * Use mailtemplate from skin defined in site
     * @return Email
     */
    public function setSiteSkin()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';
        $settings = bab_getInstance('bab_Settings');
        /*@var $settings \bab_Settings */
        $set = $settings->getSiteSettings();
        
        $this->setSkinName($set['skin']);
    }
    
    /**
     * Set a skin name to use the coresponding mail template
     * @param  string $name
     * @return Email
     *
     */
    public function setSkinName($name)
    {
        $this->_skin_mail_template = $name;
        return $this;
    }
    
    protected function getEmailTemplate($body)
    {
        if (isset($this->_skin_mail_template))
        {
            require_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';
            
            $skin = new \bab_skin($this->_skin_mail_template);
            $mailer = bab_mail();
            
            if ($skin->isAccessValid() && $mailer)
            {
                global $babSkin,$babStyle;
                
                $skinname = $babSkin;
                $stylesname = $babStyle;
                
                // set skin
                \bab_skin::applyOnCurrentPage($this->_skin_mail_template, null);
                
                $html = $mailer->mailTemplate($body);
                
                // restore skin
                if (isset($skinname)) {
                    \bab_skin::applyOnCurrentPage($skinname, $stylesname);
                }
                
                return $html;
            }
        }
        
        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
			<head></head>
			<body>
			<div align="left">' . $body . '</div>
			</body>
			</html>';
        
        return $html;
    }
    
    /**
     * Checks if the email server has been configured on the portal.
     *
     * @return bool False if the email server is not configured or not activated.
     */
    public static function emailServerIsConfigured()
    {
        global $babBody;
        
        if (empty($babBody->babsite['mailfunc'])) {
            return false;
        }
        return true;
    }
    
    /**
     * Get the upload path for files related to this email.
     *
     * @return \bab_Path
     */
    public function uploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        
        $path = $this->App()->uploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }
    
    /**
     *
     * @param string	$emailAddress
     * @return bool
     */
    static public function emailAddressIsValid($emailAddress)
    {
        return (strpos($emailAddress, '@') !== false);
    }
    
    
    public function save($noTrace = false)
    {
        // dans certains cas, on ne veut pas ecraser la variable body deja enregistree car elle n'est pas la meme que cette envoyee (mot de passe cache par des *)
        // donc on ne modifie jamais le body si il a deja ete enregistree une fois
        if ($this->id)
        {
            $this->body = null;
        }
        
        parent::save($noTrace);
    }
    
    /**
     * Sends the email.
     *
     * @return bool
     */
    public function send()
    {
        include_once $GLOBALS['babInstallPath'] . 'utilit/mailincl.php';
        
        $App = $this->App();
        $mailer = bab_mail();
        
        if (!$mailer) {
            $this->status = $App->getComponentByName('Email')->translate('Mail server not configured / activated');
            $this->save();
            return false;
        }
        
        if (!empty($this->fromaddress))
        {
            $fromname = isset($this->fromname) ? $this->fromname : '';
            $mailer->mailFrom($this->fromaddress, $fromname);
            
        } else if ($GLOBALS['BAB_SESS_LOGGED']) {
            
            $mailer->mailFrom($GLOBALS['BAB_SESS_EMAIL'], $GLOBALS['BAB_SESS_USER']);
            
            $this->fromaddress = $GLOBALS['BAB_SESS_EMAIL'];
            $this->fromname = $GLOBALS['BAB_SESS_USER'];
        }
        if (!empty($this->replyTo)) {
            $mailer->clearReplyTo();
            $mailer->mailReplyTo($this->replyTo);
        }
        $mailer->mailSubject($this->subject);
        
        $html = $this->getEmailTemplate($this->body);
        $mailer->mailBody($html, 'text/html');
        $mailer->mailAltBody($this->altbody);
        
        $mailer->clearAllRecipients();
        
        $recipients = preg_split('/[\s]*[,;][\s]*/', $this->recipients);
        foreach ($recipients as $address) {
            if (self::emailAddressIsValid($address)) {
                $mailer->mailTo($address);
            }
        }
        $recipients = preg_split('/[\s]*[,;][\s]*/', $this->ccRecipients);
        foreach ($recipients as $address) {
            if (self::emailAddressIsValid($address)) {
                $mailer->mailCc($address);
            }
        }
        $recipients = preg_split('/[\s]*[,;][\s]*/', $this->bccRecipients);
        foreach ($recipients as $address) {
            if (self::emailAddressIsValid($address)) {
                $mailer->mailBcc($address);
            }
        }
        
        if ($attachments = $this->attachments()) {
            foreach ($attachments as $attachment) {
                $mailer->mailFileAttach($attachment->getFilePath()->toString(), $attachment->toString(), '');
            }
        }
        
        $Spooler = @bab_functionality::get('Mailspooler');
        /*@var $Spooler \Func_Mailspooler */
        if ($Spooler)
        {
            $this->hash = $Spooler->save($mailer);
        }
        
        $return = $mailer->send();
        
        if ($return) {
            $this->status = 'Sent';
            $this->message_id = $mailer->getMessageId();
            
        } else {
            $this->statusMessages[] = $mailer->mail->ErrorInfo;
            $this->status = implode("\n", $this->statusMessages);
        }
        
        $this->save();
        
        $event = new EmailAfterSendEvent($this);
        bab_fireEvent($event);
        
        return $return;
    }    
    
    /**
     * Attach a file to the email.
     * If the file is provided by a filepicker widget (Widget_FilePickerItem), the file will be moved into email upload path.
     * If the file is provided with a path (bab_Path), the file will be copied into email upload path.
     *
     *
     * @see \Widget_FilePickerIterator
     *
     * @param	Widget_FilePickerItem | bab_Path	$file
     *
     *
     */
    public function attachFile($file)
    {
        $uploadPath = $this->uploadPath();
        $uploadPath->createDir();
        
        if ($file instanceof \Widget_FilePickerItem)
        {
            $original = $file->getFilePath()->toString();
            $uploadPath->push(basename($original));
            
            rename($original, $uploadPath->toString());
            
        } else if ($file instanceof \bab_Path)
        {
            // encode file into uploadPath, the source path must be encoded in ovidentia database charset
            $W = bab_Widgets();
            $filePicker = $W->FilePicker()->setFolder($uploadPath);
            $filePicker->importFile($file, \bab_Charset::getIso());
            
        } else {
            throw new \ErrorException('wrong parameter type');
        }
    }
    
    /**
     * Import data into an attachment
     *
     * @param string $filename
     * @param string $data		Binary
     */
    public function importAttachment($filename, $data)
    {
        $uploadPath = $this->uploadPath();
        
        $W = bab_Widgets();
        $filePicker = $W->FilePicker()->setFolder($uploadPath);
        $h = $filePicker->openFile($filename, \bab_charset::getIso(), 'w');
        fputs($h, $data);
        fclose($h);
    }
    
    /**
     * get iterator with attachements
     * @return \Widget_FilePickerIterator
     */
    public function attachments()
    {
        $uploadPath = $this->uploadPath();
        if(!isset($uploadPath)){
            return array();
        }
        $filePicker = bab_Widgets()->FilePicker();
        $filePickerIterator = $filePicker->getFolderFiles($uploadPath);
        return $filePickerIterator;
    }
    
    /**
     * @return array
     */
    public function getReferences()
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this);
        
        $ref = array();
        foreach ($links as $link) {
            /*@var $link \app_Link */
            $ref[] = $link->getSource()->getRef();
        }
        
        return $ref;
    }
    
    /**
     * Copy links of current email
     * @param Email $record
     */
    public function copyLinksTo(Email $record)
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this);
        
        foreach($links as $link){
            /*@var $link \app_Link */
            
            // tester si le lien existe deja
            $exists = $linkSet->select(
                $linkSet->targetClass->is(get_class($record))
                ->_AND_($linkSet->targetId->is($record->id))
                ->_AND_($linkSet->sourceClass->is($link->sourceClass))
                ->_AND_($linkSet->sourceId->is($link->sourceId))
                ->_AND_($linkSet->type->is($link->type))
            );
            
            if($exists->count() > 0){
                continue;
            }
            
            /*@var $newLink \app_Link */
            $newLink = $linkSet->newRecord();
            $newLink->targetClass = get_class($record);
            $newLink->targetId = $record->id;
            $newLink->sourceClass = $link->sourceClass;
            $newLink->sourceId = $link->sourceId;
            $newLink->type = $link->type;
            
            $newLink->save();
        }
    }
    
    /**
     * @return array of Contact
     */
    public function getLinkedContacts($linkType = null)
    {
        $App = $this->App();
        if(!$App->getComponentByName('Contact')){
            return array();
        }
        $App->includeContactSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->ContactClassName(), $linkType);
        
        $contacts = array();
        foreach ($links as $link) {
            $contacts[] = $link->sourceId;
        }
        
        return $contacts;
    }
    
    /**
     * @return array of Organization
     */
    public function getLinkedOrganizations($linkType = null)
    {
        $App = $this->App();
        if(!$App->getComponentByName('Organization')){
            return array();
        }
        $App->includeOrganizationSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->OrganizationClassName(), $linkType);
        
        $orgs = array();
        foreach ($links as $link) {
            if ($link->sourceId) {
                $orgs[] = $link->sourceId;
            }
        }
        
        return $orgs;
    }
    
    /**
     * @return array of Project
     */
    public function getLinkedProjects($linkType = null)
    {
        $App = $this->App();
        if(!$App->getComponentByName('Project')){
            return array();
        }
        $App->includeProjectSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->ProjectClassName(), $linkType);
        
        $deals = array();
        foreach ($links as $link) {
            $deals[] = $link->sourceId;
        }
        
        return $deals;
    }
    
    /**
     * @return array of Team
     */
    public function getLinkedTeams($linkType = 'requiresTask')
    {
        $App = $this->App();
        if(!$App->getComponentByName('Team')){
            return array();
        }
        $App->includeTeamSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->TeamClassName(), $linkType);
        
        $teams = array();
        foreach ($links as $link) {
            $teams[] = $link->sourceId;
        }
        
        return $teams;
    }
}
